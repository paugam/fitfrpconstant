import numpy as np 
import matplotlib.pyplot as plt
from scipy import optimize

#homebrwed
import spectralTools

reload(spectralTools)

for case in ['aqua','terra','bird', 'agema']:

    if case == 'aqua':
        srf_file = './MODIS_AQUA_Band21_RSR.txt'
        flag_srf = 'MODIS_AQUA'
        martin   = 2.98

    if case == 'terra':
        srf_file = './MODIS_TERRA_Band21_RSR.txt'
        flag_srf = 'MODIS_TERRA'
        martin   = 2.96

    if case == 'bird':
        srf_file = 'BIRD_MIR.txt'
        flag_srf = '_'
        martin   = 3.33

    if case == 'agema':
        srf_file = './AGEMA.txt'
        flag_srf = '_'
        martin   = 3.08


    channels, wavelength_SRFs, SRFs, TTRad = spectralTools.get_tabulated_TT_Rad(srf_file, 0.01, flag_srf)

    fig = plt.figure(figsize=(12,4.5))
    ax = plt.subplot(121)
    idx = np.where(channels==1)
    ax.plot(wavelength_SRFs[idx],SRFs[idx])
    ax.set_title('spectral response function {:s}'.format(case))
    ax.set_ylabel('srf')
    ax.set_xlabel('lambda')

    ax = plt.subplot(122)

    for icha in range(TTRad.shape[0]):
        ax.plot(TTRad.temperature[icha,:],TTRad.radiance[icha,:],label='{:s} radiance'.format(case))
        break
    ax.set_xlim(500,1500)
    ax.set_ylim(TTRad.radiance[0,np.abs(TTRad.temperature-500).argmin()],TTRad.radiance[0,np.abs(TTRad.temperature-1500).argmin()])
    ax.set_xlabel('T (K)')
    ax.set_ylabel('Radiance (W/m2/str)')


    def frp_fit(temp,aa):
        return aa * 1.e-9 * temp**4


    def residual(coeff,*args):
        lowT,highT,icha = args
        idx = np.where((TTRad.temperature[icha,:]>=lowT)&(TTRad.temperature[icha,:]<=highT))
        return np.sqrt((((TTRad.radiance[icha,:]-frp_fit(TTRad.temperature[icha,:],coeff))[idx])**2).sum())

    optval_arr = []
    for icha in range(TTRad.radiance.shape[0]):
        myranges_bfg = [(0.01,10)]
        coeff0 = [1.]
        #popt = optimize.fmin_l_bfgs_b( residual, coeff0, args=((650,1300)), bounds=myranges_bfg, approx_grad=True, iprint=-1, epsilon=1.e-10)
        #optval = popt[0][0]
        popt = optimize.least_squares( residual, coeff0, args=((650,1300,icha)), ) #bounds=myranges_bfg,)
        optval = popt['x'][0]
        optval_arr.append(optval)

    optval = np.array(optval_arr).mean()
    print '{:s} {:.2f} {:.2f}'.format(case, optval, martin)
    temp = np.arange(500,1500)
    ax.plot(temp,frp_fit(temp,martin),label='martin, {:.2f}'.format(martin))
    ax.plot(temp,frp_fit(temp,optval),label='opt, {:.2f}'.format(optval))
    ax.axvline(x=650, linestyle=':', c='k')
    ax.axvline(x=1300,linestyle=':', c='k')
    ax.legend()
    fig.savefig('{:s}.png'.format(case))
    plt.close(fig)


