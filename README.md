# README #

to run 
```
python fitFRPcte.py
```
it computes the FRP parameter for 4 sensors: `aqua`, `terra`, `bird`, `agema`.

### What is this repository for? ###

* It compute the parameter of the MIR FRP formulation of Wooster et al 2005.
* it uses as in Wooster at al 2005, a least square optimization and a temperature range of [650,1300] K
* Results slightly differ by ~2%, see figure below.
* I am not sure of which spectral response functions were used in Wooster et al 2005, in particular for the agema.
* same with the temperature discritization. Here, we have 1K from 100 to 2000K.

![terra](pngREADME/terra.png)
![aqua](pngREADME/aqua.png)
![bird](pngREADME/bird.png)
![agema](pngREADME/agema.png)
